package first;
public class FibonacciSeries
{
	public static void main(String[] args)
	{
	   int number1 = 0;
	   int number2 = 1;
	   int count = 0;
	   while (count<=20)
	     {
	      int sum = number1 + number2;
	      System.out.print("  "+sum);	   
          number1 =number2;
          number2 = sum;
          count++;
	     }
	}
}
